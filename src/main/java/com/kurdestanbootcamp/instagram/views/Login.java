package com.kurdestanbootcamp.instagram.views;


import com.kurdestanbootcamp.instagram.user_service.User;
import com.kurdestanbootcamp.instagram.user_service.UserProxy;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.util.List;
import java.util.stream.Stream;

@Route(value = "Login")
@PageTitle("Login | Vaadin ")
public class Login extends FormLayout {

    private H2 title;

    private TextField username;

    private PasswordField password;

    private Button loginButton;
    private Button forgotPassword;

    private Text question;
    private Button signUp;
    private Span errorMessageField;

    List<User> users;
    UserProxy userProxy;

    public Login(UserProxy userProxy) {
        this.userProxy = userProxy;
        users = userProxy.getAll();
        title = new H2("Instagram");
        username = new TextField("Username");
        username.setPrefixComponent(VaadinIcon.USER.create());


        password = new PasswordField("Password");
        password.setPrefixComponent(VaadinIcon.PASSWORD.create());

        setRequiredIndicatorVisible(username, password);

        errorMessageField = new Span();

        loginButton = new Button("Log In");
        loginButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);


        forgotPassword = new Button("Forgot password?");
        forgotPassword.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        question = new Text("Don't have an account?");
        signUp = new Button("Sign up");
        signUp.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);

        HorizontalLayout horizontalLayout = new HorizontalLayout(question,signUp);
        horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);

        VerticalLayout verticalLayout = new VerticalLayout(title,username,password,loginButton,forgotPassword,horizontalLayout);
        verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        //verticalLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);




        add(verticalLayout, errorMessageField);


        forgotPassword.addClickListener(buttonClickEvent -> {
            forgotPassword.getUI().ifPresent(ui -> ui.navigate(SignUp.class));
        });


        signUp.addClickListener(buttonClickEvent -> {
                    signUp.getUI().ifPresent(ui -> ui.navigate(SignUp.class));
                });




        loginButton.addClickListener(
                buttonClickEvent -> {

                    String username1 = username.getValue();
                    String password1 = password.getValue();

                    if (username1.isEmpty() || password1.isEmpty()) {

                        loginButton.setEnabled(false);

                        Notification notification = show("Username and password cannot be empty");
                        notification
                                .addDetachListener(detachEvent -> loginButton.setEnabled(true));
                    }
                   for (User user1 : users){

                       if (user1.getUsername().equals(username1) && user1.getPassword().equals(password1)){
                           loginButton.getUI().ifPresent(ui -> ui.navigate(Header.class, user1.getId()));
                           break;


                       } /*else if (!(username1.isEmpty()) && !(password1.isEmpty())) {
                           if (!Objects.equals(userDTO1.getUsername(), username1) || !Objects.equals(userDTO1.getPassword(), password1)){

                               loginButton.setEnabled(false);

                               Notification notification = show("incorrect");
                               notification
                                       .addDetachListener(detachEvent -> loginButton.setEnabled(true));
                               break;
                           }

                       }*/
                   }

                }
        );
    }

    private void setRequiredIndicatorVisible(HasValueAndElement<?, ?>... components) {
        Stream.of(components).forEach(comp -> comp.setRequiredIndicatorVisible(true));
    }

    public Notification show(String message) {
        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

        Div text = new Div(new Text(message));

        Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });

        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        notification.add(layout);
        notification.open();

        notification.setPosition(Notification.Position.TOP_CENTER);
        return notification;
    }
}
