package com.kurdestanbootcamp.instagram.views;

import com.kurdestanbootcamp.instagram.follower_service.FollowerDTO;
import com.kurdestanbootcamp.instagram.follower_service.FollowerProxy;
import com.kurdestanbootcamp.instagram.following_service.FollowingDTO;
import com.kurdestanbootcamp.instagram.following_service.FollowingProxy;
import com.kurdestanbootcamp.instagram.user_service.User;
import com.kurdestanbootcamp.instagram.user_service.UserProxy;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.dom.ElementFactory;
import com.vaadin.flow.router.*;

import java.util.List;

@Route(value = "Home", layout = Header.class)
@PageTitle("Home | Vaadin ")
public class Home extends Div implements HasUrlParameter<String> {

  UserProxy userProxy;
  FollowerProxy followerProxy;
  FollowingProxy followingProxy;
  FollowingDTO followingDTO;
  FollowerDTO followerDTO;
  private List<User> users;

  TextField userId;

  Button follow;

  //Long followingUserId;

  MultiSelectListBox<User> listBox;

   ComponentRenderer<Component, User> userCardRenderer;

  public Home(UserProxy userProxy, FollowerProxy followerProxy, FollowingProxy followingProxy) {
    userId = new TextField();
    this.userProxy = userProxy;
    this.followerProxy = followerProxy;
    this.followingProxy = followingProxy;
    followingDTO = new FollowingDTO();
    followerDTO = new FollowerDTO();
    /*follow = new Button("Follow");
    follow.addThemeVariants(ButtonVariant.LUMO_PRIMARY);*/
    users = userProxy.getAll();
    //followingUserId = null;



    userCardRenderer = new ComponentRenderer<>(userDTO -> {

      //followingUserId = userDTO.getId();
      HorizontalLayout cardLayout = new HorizontalLayout();
      cardLayout.setMargin(true);

      Avatar avatar = new Avatar(userDTO.getFullName(), userDTO.getProfilePhoto());
      avatar.setHeight("64px");
      avatar.setWidth("64px");

      VerticalLayout infoLayout = new VerticalLayout();
      infoLayout.setSpacing(false);
      infoLayout.setPadding(false);
      infoLayout.getElement().appendChild(ElementFactory.createStrong(userDTO.getUsername()));
      infoLayout.add(new Div(new Text(userDTO.getFullName())));


      follow = new Button("Follow");
      follow.addThemeVariants(ButtonVariant.LUMO_PRIMARY);


      cardLayout.add(avatar, infoLayout, follow);
      return cardLayout;
    });




    listBox = new MultiSelectListBox<>();
    listBox.setItems(users);
    listBox.setRenderer(userCardRenderer);


    add(listBox);


  }

  @Override
  public void setParameter(BeforeEvent beforeEvent, String parameter) {

    if (parameter == null) {
      System.out.println("null");
    } else {
      userId.setValue(String.format(parameter));

      Long userId = Long.parseLong(parameter);

      User user = userProxy.getById(userId);

      follow.addClickListener(buttonClickEvent ->{

      });


      listBox.select();
      listBox.addSelectionListener(buttonClickEvent ->{
      /*  var x = user.getId();
        var userDTO1 = userProxy.getById(x);*/

        buttonClickEvent.getFirstSelectedItem().ifPresent(user1 -> {
          Long idd = user1.getId();
          System.out.println("user"+ idd);

          User user2 = userProxy.getById(idd);

          followingDTO.setUser(user2);
          followingProxy.save(followingDTO);


          followerDTO.setUser(user);
          followerProxy.save(followerDTO);
                });

      });

    }
  }

}