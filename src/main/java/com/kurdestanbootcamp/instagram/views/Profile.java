package com.kurdestanbootcamp.instagram.views;

import com.kurdestanbootcamp.instagram.follower_service.FollowerDTO;
import com.kurdestanbootcamp.instagram.follower_service.FollowerProxy;
import com.kurdestanbootcamp.instagram.following_service.FollowingDTO;
import com.kurdestanbootcamp.instagram.following_service.FollowingProxy;
import com.kurdestanbootcamp.instagram.minio.MinioProxy;
import com.kurdestanbootcamp.instagram.user_service.User;
import com.kurdestanbootcamp.instagram.user_service.UserProxy;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.ShortcutRegistration;
import com.vaadin.flow.component.Shortcuts;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.InputStreamFactory;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.compress.utils.IOUtils;

import javax.imageio.ImageIO;
import javax.xml.transform.stream.StreamSource;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.Objects;

@Route(value = "Profile", layout = Header.class )
@PageTitle("Profile | Vaadin ")
public class Profile extends VerticalLayout  implements HasUrlParameter<String> {

    UserProxy userProxy;
    FollowerProxy followerProxy;
    FollowingProxy followingProxy;
    User user;
    TextField userId;

    private Avatar image;

   // Avatar image;
    private H2 userName;
    private Button editProfile;

    private Button postBtn;
    private Button followerBtn;
    private Button followingBtn;
    private Text postCountText;
    private Text followerCountText;
    private Text followingCountText;
    private H5 fullName;

    MinioProxy minioProxy;





    @Override
    public void setParameter(BeforeEvent beforeEvent, String parameter) {

        if (parameter == null) {
            System.out.println("null");
        } else {
            userId.setValue(String.format(parameter));

            Long id = Long.parseLong(parameter);

            User user1 = userProxy.getById(id);

            fullName.setText(user1.getFullName());
            userName.setText(user1.getUsername());

            String image1 = user1.getProfilePhoto();
           /*  minioProxy.downloadFile(image1);

            //image.setImage();
            var file = minioProxy.downloadFile(image1);*/
            image.setImage("icons/images/profile.jpg");


            List<FollowerDTO> followerDTOS = followerProxy.getAllByUser(id);
            followerCountText.setText(String.valueOf(followerDTOS.size()));

            List<FollowingDTO> followingDTOS = followingProxy.getAllByUser(id);
            followingCountText.setText(String.valueOf(followingDTOS.size()));



            followerBtn.addClickListener(buttonClickEvent -> {



                Followers followers = new Followers(userProxy, followerDTOS, followerProxy);
                add(followers);
            });

            followingBtn.addClickListener(buttonClickEvent -> {

                Following following = new Following(userProxy, followingDTOS, followingProxy);
                add(following);
            });


        /*    image.addClickListener(imageClickEvent -> {

                UploadImageToFile uploadImageToFile = new UploadImageToFile();
                add(uploadImageToFile);

                //UploadImage();
            });*/

        }
    }





    public void UploadImage() {
        MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
        Upload upload = new Upload(buffer);

        upload.addSucceededListener(event -> {
            String fileName = event.getFileName();
            InputStream inputStream = buffer.getInputStream(fileName);

            // Do something with the file data
            // processFile(inputStream, fileName);
        });


        add(upload);
    }



    public Profile(UserProxy userProxy, FollowingProxy followingProxy, FollowerProxy followerProxy, MinioProxy minioProxy) {

        userId = new TextField();

        this.userProxy = userProxy;
        this.followingProxy = followingProxy;
        this.followerProxy = followerProxy;
        this.minioProxy = minioProxy;
        user = new User();
       //id = new TextField();

        /*image = new Image("icons/images/profile.jpg", "Profile");
        image.setWidth("150px");
        image.setHeight("150px");*/

        image = new Avatar();
        image.setWidth("150px");
        image.setHeight("150px");

        userName = new H2("");

        editProfile = new Button("Edit Profile");
        editProfile.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        postBtn = new Button("posts");
        postBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        followingBtn = new Button("followings");
        followingBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        followerBtn = new Button("followers");
        followerBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        postCountText = new Text("");
        followerCountText = new Text("");
        followingCountText = new Text("");


        fullName = new H5("");
        //fullName.setText(this.userDTO.getFullName());


        HorizontalLayout horizontalLayout = new HorizontalLayout(userName, editProfile);
        horizontalLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
        horizontalLayout.setWidth("100%");
        horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        HorizontalLayout horizontalLayout2 = new HorizontalLayout(postCountText, postBtn);
        horizontalLayout2.setWidth("100%");
        HorizontalLayout horizontalLayout3 = new HorizontalLayout(followerCountText,followerBtn);
        horizontalLayout3.setWidth("100%");
        HorizontalLayout horizontalLayout4 = new HorizontalLayout(followingCountText, followingBtn);
        horizontalLayout4.setWidth("100%");
        HorizontalLayout horizontalLayout5 = new HorizontalLayout(horizontalLayout2, horizontalLayout3, horizontalLayout4);
        horizontalLayout5.setJustifyContentMode(JustifyContentMode.BETWEEN);
        horizontalLayout5.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        horizontalLayout5.setWidth("100%");
        //horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        VerticalLayout verticalLayout = new VerticalLayout(horizontalLayout,horizontalLayout5,fullName);

        HorizontalLayout horizontalLayout6 = new HorizontalLayout(image, verticalLayout);
        horizontalLayout6.setJustifyContentMode(JustifyContentMode.BETWEEN);
        horizontalLayout6.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        horizontalLayout6.setWidth("100%");


        add(horizontalLayout6/*, userId*/);


     /*   followingBtn.addClickListener(buttonClickEvent -> {

            Dialog dialog = new Dialog();
            dialog.setHeaderTitle("User details");

            Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> dialog.close());
            closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
            dialog.getHeader().add(closeButton);
            dialog.open();
        });*/




    }
    private void showDialog() {
        Button okButton = new Button("OK");
        Button cancelButton = new Button("Cancel");
        HorizontalLayout buttons = new HorizontalLayout(okButton, cancelButton);

        Dialog dialog = new Dialog(new Span("Dialog content goes here"), buttons);

        dialog.setCloseOnEsc(true);
        cancelButton.addClickListener(event -> dialog.close());

        okButton.addClickListener(
                event -> {
                    Notification.show("Accepted");
                    dialog.close();
                }
        );
        okButton.addClickShortcut(Key.ENTER);

        // Prevent click shortcut of the OK button from also triggering when
        // another button is focused
        ShortcutRegistration shortcutRegistration = Shortcuts
                .addShortcutListener(buttons, () -> {}, Key.ENTER)
                .listenOn(buttons);
        shortcutRegistration.setEventPropagationAllowed(false);
        shortcutRegistration.setBrowserDefaultAllowed(true);

        dialog.open();
    }

}
