package com.kurdestanbootcamp.instagram.views;

import com.kurdestanbootcamp.instagram.user_service.UserProxy;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Main")
@Route(value = "")
public class MainLayout extends FormLayout{
    UserProxy userProxy;
    public MainLayout(UserProxy userProxy) {
        this.userProxy = userProxy;
        Login login = new Login(userProxy);
        add(login);
    }


}
