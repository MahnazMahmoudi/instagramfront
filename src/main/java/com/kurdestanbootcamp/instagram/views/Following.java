package com.kurdestanbootcamp.instagram.views;

import com.kurdestanbootcamp.instagram.follower_service.FollowerDTO;
import com.kurdestanbootcamp.instagram.follower_service.FollowerProxy;
import com.kurdestanbootcamp.instagram.following_service.FollowingDTO;
import com.kurdestanbootcamp.instagram.following_service.FollowingProxy;
import com.kurdestanbootcamp.instagram.user_service.User;
import com.kurdestanbootcamp.instagram.user_service.UserProxy;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.dom.ElementFactory;
import com.vaadin.flow.router.Route;

import java.util.List;

@Route("following")
public class Following extends Div {

    //final UserDTO user = UserProxy.getPeople().get(0);

    User user;
    UserProxy userProxy;
    FollowingProxy followingProxy;

    private Image image;

    private Text following;
    private Text description;
    private Text title;
    Button follow;
    Button remove;

    private List<User> users;
    private List<FollowingDTO> followingDTOS;

    ComponentRenderer<Component, User> userCardRenderer;

    ComponentRenderer<Component, FollowingDTO> followingCardRenderer;
    MultiSelectListBox<User> listBox;
    MultiSelectListBox<FollowingDTO> listBox2;

    public Following(UserProxy userProxy, List<FollowingDTO> followingDTOS, FollowingProxy followingProxy) {
        this.userProxy = userProxy;
        this.followingProxy = followingProxy;

       this.followingDTOS = followingDTOS;

        users = userProxy.getAll();

        image = new Image("icons/images/folower.png", "follower");
        image.setWidth("80px");
        image.setHeight("80px");
        following = new Text("People you follow");
        description = new Text("Once you follow people,you'll see them here.");
        title = new Text("Suggestions For You");



        followingCardRenderer = new ComponentRenderer<>(followingDTO -> {
            //followingUserId = userDTO.getId();
            HorizontalLayout cardLayout = new HorizontalLayout();
            cardLayout.setMargin(true);
            /*var id =followerDTO.getUserId();
            UserDTO userDTO1 = userProxy.getById(id);*/
            var id = followingDTO.getId();

            //var userId = followerDTO.getUserId();
            User user1 = userProxy.getById(124l);




            Avatar avatar = new Avatar(user1.getFullName(), user1.getProfilePhoto());
            avatar.setHeight("64px");
            avatar.setWidth("64px");

            VerticalLayout infoLayout = new VerticalLayout();
            infoLayout.setSpacing(false);
            infoLayout.setPadding(false);
            infoLayout.getElement().appendChild(ElementFactory.createStrong(user1.getUsername()));
            infoLayout.add(new Div(new Text(user1.getFullName())));


            remove = new Button("Remove");
            remove.addThemeVariants(ButtonVariant.MATERIAL_OUTLINED);


            cardLayout.add(avatar, infoLayout, remove);
            cardLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
            return cardLayout;
        });


        userCardRenderer = new ComponentRenderer<>(userDTO -> {
            //followingUserId = userDTO.getId();
            HorizontalLayout cardLayout = new HorizontalLayout();
            cardLayout.setMargin(true);

            Avatar avatar = new Avatar(userDTO.getFullName(), userDTO.getProfilePhoto());
            avatar.setHeight("64px");
            avatar.setWidth("64px");

            VerticalLayout infoLayout = new VerticalLayout();
            infoLayout.setSpacing(false);
            infoLayout.setPadding(false);
            infoLayout.getElement().appendChild(ElementFactory.createStrong(userDTO.getUsername()));
            infoLayout.add(new Div(new Text(userDTO.getFullName())));


            follow = new Button("Follow");
            follow.addThemeVariants(ButtonVariant.LUMO_PRIMARY);


            cardLayout.add(avatar, infoLayout, follow);
            cardLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
            return cardLayout;
        });




        listBox = new MultiSelectListBox<>();
        listBox.setItems(users);
        listBox.setRenderer(userCardRenderer);

        listBox2 = new MultiSelectListBox<>();
        listBox2.setItems(followingDTOS);
        listBox2.setRenderer(followingCardRenderer);

        //add(listBox);



        Dialog dialog = new Dialog();
        dialog.getElement().setAttribute("aria-label", "Add note");

        VerticalLayout dialogLayout = createDialogLayout(dialog);
        if(followingDTOS.size() == 0){
            dialog.add(dialogLayout, title, listBox);
        }else {
            dialog.add(listBox2, title, listBox);
        }

        dialog.setHeaderTitle("Following");

        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> dialog.close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        dialog.getHeader().add(closeButton);

        dialog.open();
        add(dialog);
    }

    private VerticalLayout createDialogLayout(Dialog dialog) {


        VerticalLayout verticalLayout = new VerticalLayout(image, following,description);
        //VerticalLayout verticalLayout = new VerticalLayout(verticalLayout1, title);


        return verticalLayout;
    }

}