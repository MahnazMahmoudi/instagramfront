package com.kurdestanbootcamp.instagram.views;

import com.kurdestanbootcamp.instagram.user_service.User;
import com.kurdestanbootcamp.instagram.user_service.UserProxy;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


import java.util.stream.Stream;

@Route(value = "SignUp")
@PageTitle("SignUp | Vaadin ")
public class SignUp extends FormLayout {

    UserProxy userProxy;

    private H2 title;
    private H5 description;

    private TextField phoneNumber;
    private TextField fullName;
    private TextField username;

    private PasswordField password;
   // private PasswordField passwordConfirm;

    private Span errorMessageField;

    private Button signUpButton;

    private Icon checkIcon;
    private Span passwordStrengthText;

    public SignUp(UserProxy userProxy) {
        this.userProxy = userProxy;
        title = new H2("Instagram");
        description = new H5("Sign up to see photos and videos from your friends.");

        phoneNumber = new TextField("Mobile Number");
        phoneNumber.setPrefixComponent(VaadinIcon.PHONE.create());
        phoneNumber.setMaxLength(11);
        phoneNumber.setHelperText("Max 11 characters");


        fullName = new TextField("Full name");
        fullName.setPrefixComponent(VaadinIcon.USER.create());


        username = new TextField("Username");
        username.setPrefixComponent(VaadinIcon.USER.create());



        password = new PasswordField("Password");
        password.setPrefixComponent(VaadinIcon.PASSWORD.create());



        checkIcon = VaadinIcon.CHECK.create();
        checkIcon.setVisible(false);
        checkIcon.getStyle().set("color", "var(--lumo-success-color)");
        password.setSuffixComponent(checkIcon);

        Div passwordStrength = new Div();
        passwordStrengthText = new Span();
        passwordStrength.add(new Text("Password strength: "),
                passwordStrengthText);
        password.setHelperComponent(passwordStrength);



        //passwordConfirm = new PasswordField("Confirm password");

        setRequiredIndicatorVisible(phoneNumber, fullName, username, password);

        errorMessageField = new Span();

        signUpButton = new Button("Sign up");
        signUpButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        VerticalLayout verticalLayout = new VerticalLayout(title, description,phoneNumber,fullName,
                username,password,signUpButton);
        verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        verticalLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        verticalLayout.setAlignSelf(FlexComponent.Alignment.CENTER);
        add(verticalLayout, errorMessageField);

        password.setValueChangeMode(ValueChangeMode.EAGER);
        password.addValueChangeListener(e -> {
            String password = e.getValue();
            updateHelper(password);
        });

        updateHelper("");

        signUpButton.addClickListener(buttonClickEvent -> {
            String phoneNumberValue = phoneNumber.getValue();
            String fullNameValue = fullName.getValue();
            String userNameValue = username.getValue();
            String passwordValue = password.getValue();
            if(!(phoneNumberValue.isEmpty()) && !(fullNameValue.isEmpty())
                    && !(userNameValue.isEmpty()) && !(passwordValue.isEmpty())) {

                User user = new User();
                user.setPhoneNumber(phoneNumberValue);
                user.setFullName(fullNameValue);
                user.setUsername(userNameValue);
                user.setPassword(passwordValue);
                //userDTO.setId(userDTO.getId());
                userProxy.save(user);
                User user1 = userProxy.getByUsername(user.getUsername());
                Long userId = user1.getId();

                System.out.println("success done");
                Notification notification = Notification.show("Welcome to instagram");
                notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

                signUpButton.getUI().ifPresent(ui -> ui.navigate(Header.class, userId));


            }else {
                signUpButton.setEnabled(false);

                Notification notification = show("not empty");
                notification
                        .addDetachListener(detachEvent -> signUpButton.setEnabled(true));
            }


        });


        // Max width of the Form
        setMaxWidth("500px");
    }


    private void setRequiredIndicatorVisible(HasValueAndElement<?, ?>... components) {
        Stream.of(components).forEach(comp -> comp.setRequiredIndicatorVisible(true));
    }

    public Notification show(String message) {
        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

        Div text = new Div(new Text(message));

        Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });

        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        notification.add(layout);
        notification.open();

        notification.setPosition(Notification.Position.TOP_CENTER);
        return notification;
    }

    private void updateHelper(String password) {
        if (password.length() > 9) {
            passwordStrengthText.setText("strong");
            passwordStrengthText.getStyle().set("color",
                    "var(--lumo-success-color)");
            checkIcon.setVisible(true);
        } else if (password.length() > 5) {
            passwordStrengthText.setText("moderate");
            passwordStrengthText.getStyle().set("color", "#e7c200");
            checkIcon.setVisible(false);
        } else {
            passwordStrengthText.setText("weak");
            passwordStrengthText.getStyle().set("color",
                    "var(--lumo-error-color)");
            checkIcon.setVisible(false);
        }
    }
}
