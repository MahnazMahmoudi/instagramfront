package com.kurdestanbootcamp.instagram.views;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.*;


@Route(value = "Main")
@PageTitle("Main | Vaadin ")
public class Header extends AppLayout implements HasUrlParameter<Long> {

    TextField userId;


    public Header() {
        userId = new TextField();

        createHeader();
    }


    @Override
    public void setParameter(BeforeEvent beforeEvent, Long parameter) {

        if (parameter == null) {
            System.out.println("null");
        } else {
            userId.setValue(String.format(parameter.toString()));
        }
    }



    private void createHeader() {


       // id = new TextField();
        H1 logo = new H1("instagram");
        logo.addClassNames("text-l", "m-m");

        MenuBar menuBar = new MenuBar();
        menuBar.addThemeVariants(MenuBarVariant.LUMO_TERTIARY_INLINE);

        var home = createIconItem(menuBar, VaadinIcon.HOME_O, "Home");
        createIconItem(menuBar, VaadinIcon.PLUS_SQUARE_O, "Plus");
        createIconItem(menuBar, VaadinIcon.HEART_O, "Heart");
        createIconItem(menuBar, VaadinIcon.COMMENT_O, "Comment");

        MenuItem user = createIconItem(menuBar, VaadinIcon.USER, "User");
        SubMenu userSubMenu = user.getSubMenu();
        var profile = userSubMenu.addItem("Profile");
        var saved =userSubMenu.addItem("Saved");
        var settings = userSubMenu.addItem("Settings");
        var report =userSubMenu.addItem("Report a problem");
        var switchAccounts =userSubMenu.addItem("Switch accounts");
        var logOut =userSubMenu.addItem("Log Out");


        TextField textField = new TextField();
        textField.setPlaceholder("Search");
        textField.setPrefixComponent(VaadinIcon.SEARCH.create());
        textField.setClearButtonVisible(true);
        textField.setWidth("22%");

        HorizontalLayout header = new HorizontalLayout(
                logo, textField,menuBar
        );

        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.setJustifyContentMode(FlexComponent.JustifyContentMode.AROUND);
        header.setWidth("100%");
        header.addClassNames("py-0", "px-m");


        addToNavbar(header);
        profile.addClickListener(buttonClickEvent -> {
            profile.getUI().ifPresent(ui -> ui.navigate(Profile.class,userId.getValue()));
        });

        home.addClickListener(buttonClickEvent -> {
            home.getUI().ifPresent(ui -> ui.navigate(Home.class, userId.getValue()));
        });


    }


    private MenuItem createIconItem(MenuBar menu, VaadinIcon iconName, String ariaLabel) {
        Icon icon = new Icon(iconName);
        MenuItem item = menu.addItem(icon);
        item.getElement().setAttribute("aria-label", ariaLabel);

        return item;
    }


}
