package com.kurdestanbootcamp.instagram.post_service;

import com.kurdestanbootcamp.instagram.common.PagingData;
import com.kurdestanbootcamp.instagram.user_service.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "post",url = "http://localhost:8099/post")
public interface PostProxy {

    @PostMapping("/v1")
    void save(@RequestBody User user);

    @PutMapping("/v1")
    void update(@RequestBody User user);

    @DeleteMapping("/v1/{id}")
    void delete(@PathVariable Long id);

    @GetMapping("/v1/paging/{page}/{size}")
    PagingData<User> getAll(@PathVariable Integer page, @PathVariable Integer size);

    @GetMapping("/v1/{id}")
    User getById(@PathVariable Long id);



}
