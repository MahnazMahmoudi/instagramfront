package com.kurdestanbootcamp.instagram.post_service;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PostDTO {

    private String text;

    @NotNull
    private String image;

    @NotNull
    private Long numberOfLike;

    @NotNull
    private Long numberOfComment;

}
