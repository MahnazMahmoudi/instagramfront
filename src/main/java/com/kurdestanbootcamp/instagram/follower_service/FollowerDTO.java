package com.kurdestanbootcamp.instagram.follower_service;

import com.kurdestanbootcamp.instagram.user_service.User;
import lombok.Data;



@Data
public class FollowerDTO {

    private Long id;

    private User user;

}
