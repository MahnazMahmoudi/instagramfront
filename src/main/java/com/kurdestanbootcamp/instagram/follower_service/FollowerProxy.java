package com.kurdestanbootcamp.instagram.follower_service;

import com.kurdestanbootcamp.instagram.common.PagingData;
import com.kurdestanbootcamp.instagram.common.SearchCriteria;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name = "follower",url = "http://localhost:8099/follower")
public interface FollowerProxy {

    @PostMapping("/v1")
    void save(@RequestBody FollowerDTO followerDTO);

    @PutMapping("/v1")
    void update(@RequestBody FollowerDTO followerDTO);

    @DeleteMapping("/v1/{id}")
    void delete(@PathVariable Long id);

    @GetMapping("/v1/paging/{page}/{size}")
    PagingData<FollowerDTO> getAll(@PathVariable Integer page, @PathVariable Integer size);

    @GetMapping("/v1/{id}")
    FollowerDTO getById(@PathVariable Long id);

    @GetMapping("/v1/get-all-by-user/{userId}")
    List<FollowerDTO> getAllByUser(@PathVariable Long userId);

    @GetMapping("/v1/get-all-by-user-with-paging/{userId}/{page}/{size}")
    PagingData<FollowerDTO> getAllByUser(@PathVariable Long userId, @PathVariable Integer page, @PathVariable Integer size);

    @PostMapping("/v1/search")
    List<FollowerDTO> search(@RequestBody List<SearchCriteria> searchCriteria);





}