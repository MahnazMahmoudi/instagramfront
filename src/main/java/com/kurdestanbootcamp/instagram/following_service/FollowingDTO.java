package com.kurdestanbootcamp.instagram.following_service;


import com.kurdestanbootcamp.instagram.user_service.User;
import lombok.Data;

@Data
public class FollowingDTO{

    private Long id;

    private User user;

}
