package com.kurdestanbootcamp.instagram.following_service;

import com.kurdestanbootcamp.instagram.common.PagingData;
import com.kurdestanbootcamp.instagram.common.SearchCriteria;
import com.kurdestanbootcamp.instagram.follower_service.FollowerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name = "following",url = "http://localhost:8099/following")
public interface FollowingProxy {

    @PostMapping("/v1")
    void save(@RequestBody FollowingDTO followingDTO);

    @PutMapping("/v1")
    void update(@RequestBody FollowingDTO followingDTO);

    @DeleteMapping("/v1/{id}")
    void delete(@PathVariable Long id);

    @GetMapping("/v1/paging/{page}/{size}")
    PagingData<FollowingDTO> getAll(@PathVariable Integer page, @PathVariable Integer size);

    @GetMapping("/v1/{id}")
    FollowingDTO getById(@PathVariable Long id);

    @GetMapping("/v1/get-all-by-user/{userId}")
    List<FollowingDTO> getAllByUser(@PathVariable Long userId);

    @GetMapping("/v1/get-all-by-user-with-paging/{userId}/{page}/{size}")
    PagingData<FollowingDTO> getAllByUser(@PathVariable Long userId, @PathVariable Integer page, @PathVariable Integer size);

    @PostMapping("/v1/search")
    List<FollowingDTO> search(@RequestBody List<SearchCriteria> searchCriteria);





}