package com.kurdestanbootcamp.instagram.minio;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;


@FeignClient(name = "minio",url = "http://localhost:8099")
public interface MinioProxy {

    @PostMapping("/upload")
    Map<String, String> uploadFile(@RequestPart(value = "file", required = false) MultipartFile files);

    @GetMapping("/download")
    ByteArrayResource downloadFile(@RequestParam(value = "file") String file);


}