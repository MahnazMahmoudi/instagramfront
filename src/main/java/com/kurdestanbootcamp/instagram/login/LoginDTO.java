package com.kurdestanbootcamp.instagram.login;


import com.kurdestanbootcamp.instagram.common.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginDTO extends BaseDTO {

    @NotNull
    private String username;

    @NotNull
    private String password;


}
