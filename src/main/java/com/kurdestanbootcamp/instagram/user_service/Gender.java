package com.kurdestanbootcamp.instagram.user_service;

public enum Gender {
    FEMALE,
    MALE
}
