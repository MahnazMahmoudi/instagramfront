package com.kurdestanbootcamp.instagram.user_service;

import com.kurdestanbootcamp.instagram.common.PagingData;
import com.kurdestanbootcamp.instagram.common.SearchCriteria;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name = "user",url = "http://localhost:8099/user")
public interface UserProxy {

    @PostMapping("/v1")
    void save(@RequestBody User user);

    @PutMapping("/v1")
    void update(@RequestBody User user);

    @DeleteMapping("/v1/{id}")
    void delete(@PathVariable Long id);

    @GetMapping("/v1/paging/{page}/{size}")
    PagingData<User> paging(@PathVariable Integer page, @PathVariable Integer size);

    @GetMapping("/v1")
    List<User> getAll();

    @GetMapping("/v1/{id}")
    User getById(@PathVariable Long id);

    @GetMapping("/v1/get-by-username/{username}")
    User getByUsername(@PathVariable String username);

    @PostMapping("/v1/search")
    List<User> search(@RequestBody List<SearchCriteria> searchCriteria);





}