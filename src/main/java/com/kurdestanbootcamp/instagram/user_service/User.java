package com.kurdestanbootcamp.instagram.user_service;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class User {

    private Long id;

    @NotNull
    private String fullName;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String phoneNumber;

    private String profilePhoto;

    private String bio;

    private String email;

    private Gender gender;
}
